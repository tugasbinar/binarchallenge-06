const express = require("express")
const router = express.Router()
const { body, validationResult } = require("express-validator");
const User_gameController = require('../controller/controller_user_game')

router.get('/', User_gameController.list)
router.get('/:id', User_gameController.getById)
router.post('/',
[
    body('username').notEmpty(),
    body('password').notEmpty()
],
(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        throw {
            status: 400,
            message: errors.array()
        }
    } else {
        next()
    }
},
User_gameController.create)
router.post('/login', User_gameController.login)
router.put('/:id', User_gameController.update)
router.delete('/:id', User_gameController.delete)

module.exports = router;