const router = require("express").Router();
const { body, validationResult } = require("express-validator");
const jwt = require('jsonwebtoken')
const { User_game } = require('../models')
const User_game_biodataController = require('../controller/controller_user_game_biodata')

router.get('/', User_game_biodataController.list)
router.get('/:id',
(req, res, next) => {
    if (req.headers.authorization) {
      const user_game = jwt.decode(req.headers.authorization)
      req.user_game = user_game
      next()
    } else {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    }
},
User_game_biodataController.getById)
router.post('/',
(req, res, next) => {
  if (req.headers.authorization) {
    const user_game = jwt.decode(req.headers.authorization)
    req.user_game = user_game
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
[
  body('name').notEmpty(),
  body('date_of_birth').notEmpty(),
  body('email').notEmpty(),
  body('gender').notEmpty(),
  body('country').notEmpty(),
  body('user_game_id').notEmpty()
  .custom(async value => {
    const user_game = await User_game.findOne({
      where: {
        id: value
      }
    })
    if (!user_game) {
      throw new Error ('User_game not available')
    }
    return true
  })
],
(req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors.array() 
      }
    } else {
      next()
    }
},
User_game_biodataController.create)
router.put('/:id',
(req, res, next) => {
  if (req.headers.authorization) {
    const user_game = jwt.decode(req.headers.authorization)
    req.user_game = user_game
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
[
  body('name').optional().notEmpty(),
  body('date_of_birth').optional().notEmpty(),
  body('email').optional().notEmpty(),
  body('gender').optional().notEmpty(),
  body('country').optional().notEmpty(),
  body('user_game_id').optional().notEmpty()
  .custom(async value => {
    const user_game = await User_game.findOne({
      where: {
        id: value
      }
    })
    if (!user_game) {
      throw new Error ('User_game not available')
    }
    return true
  })
],
(req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors.array() 
      }
    } else {
      next()
    }
},
User_game_biodataController.update)
router.delete('/:id',
(req, res, next) => {
  if (req.headers.authorization) {
    const user_game = jwt.decode(req.headers.authorization)
    req.user_game = user_game
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
User_game_biodataController.delete)

module.exports = router;