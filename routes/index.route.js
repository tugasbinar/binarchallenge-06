const express = require('express')
const router = express.Router();
const user_gameRoutes = require('./user_game.route')
const user_game_biodataRoutes = require('./user_game_biodata.route')
const user_game_historieRoutes = require('./user_game_historie.route')

router.use('/user_game', user_gameRoutes);
router.use('/user_game_biodata', user_game_biodataRoutes);
router.use('/user_game_historie', user_game_historieRoutes);

module.exports = router