'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_historie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_historie.belongsTo(models.User_game, { foreignKey: 'user_game_id'})
    }
  }
  User_game_historie.init({
    time_list: DataTypes.TIME,
    score: DataTypes.INTEGER,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_game_historie',
  });
  return User_game_historie;
};