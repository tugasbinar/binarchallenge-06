'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_biodata.belongsTo(models.User_game, { foreignKey: 'user_game_id' })
    }
  }
  User_game_biodata.init({
    name: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    email: DataTypes.STRING,
    gender: DataTypes.STRING,
    country: DataTypes.STRING,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_game_biodata',
  });
  return User_game_biodata;
};