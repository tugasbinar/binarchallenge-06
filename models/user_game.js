'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  class User_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game.hasOne(models.User_game_biodata, { foreignKey: 'user_game_id' })
      User_game.hasMany(models.User_game_historie, { foreignKey: 'user_game_id'})
    }
  }
  User_game.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_game',
  });
  User_game.addHook('beforeCreate', (user_game, options) => {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(user_game.password, salt);
    user_game.password = hash;
  });
  return User_game;
};