const {User_game} = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

class User_gameController {
    
    static async list(req, res, next) {
        User_game.findAll( { attributes: ['id', 'username', 'password'] } )
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            res.status(500).json(error)
        })
    }
    static async getById(req, res, next) {
        User_game.findOne( { where:{ id: req.params.id } } )
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            res.status(500).json(error)
        })
    }
    static async create(req, res, next) {
        await User_game.create(req.body)
        res.status(201).json({
            message: 'Succesfully create user_game'
        });
    }
    static async login (req, res, next) {
      try {
        const user_game = await User_game.findOne({
          where: {
            username: req.body.username
          }
        })
  
        if (!user_game) {
          throw {
            status: 401,
            message: 'Invalid username or password'
          }
        }
        if (bcrypt.compareSync(req.body.password, user_game.password)) {
          const token = jwt.sign({
            id: user_game.id,
            username: user_game.username
          }, 'qweqwe')
  
          res.status(200).json({
            token
          })
        } else {
          throw {
            status: 401,
            message: 'Invalid username or password'
          }
        }
      } catch (err) {
        next(err)
      }
    }
    static async update(req, res, next) {
        try {
            const user_game = await User_game.findOne({
              where: {
                id: req.params.id
              }
            })
      
            if (!user_game) {
              throw {
                status: 404,
                message: 'User_game not found'
              }
            } else {
              await User_game.update(req.body, {
                where: {
                  id: req.params.id
                }
              })
              res.status(200).json({
                message: 'Successfully update user_game'
              })
            }
          } catch (err) {
            next(err)
          }
    }
    static async delete(req, res, next) {
        try {
            const user_game = await User_game.findOne({
              where: {
                id: req.params.id
              }
            })
            if (!user_game) {
              throw {
                status: 404,
                message: 'User_game not found'
              }
            } else {
              await User_game.destroy({
                where: {
                    id: req.params.id
                }
              })
              res.status(200).json({
                message: 'Succesfully delete user_game'
              })
            }
          } catch (err) {
            next(err)
          }
    }
}

module.exports = User_gameController