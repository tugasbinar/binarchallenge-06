const {User_game_biodata} = require('../models')

class User_game_biodataController {
    
    static async list(req, res, next) {
      User_game_biodata.findAll( { attributes: [ 'id', 'name', 'date_of_birth', 'email', 'gender', 'country', 'user_game_id'] } )
      .then((data) => {
          res.status(200).json(data)
      })
      .catch((error) => {
          res.status(500).json(error)
      })
    }
    static async getById(req, res, next) {
        try {
            const user_game_biodata = await User_game_biodata.findOne({
              where: {
                  id: req.params.id,
                  user_game_id: req.user_game.id
              }
            })
            console.log(user_game_biodata)
            if (!user_game_biodata) {
              throw {
                status: 404,
                message: 'User_game_biodata not found'
              }
            } else {
              res.status(200).json(user_game_biodata)
            }
          } catch (err) {
            next(err)
          }
    }
    static async create(req, res, next) {
      try {
        await User_game_biodata.create({
            name: req.body.name,
            date_of_birth: req.body.date_of_birth,
            email: req.body.email,
            gender: req.body.gender,
            country: req.body.country,
            user_game_id: req.user_game.id
        })
        res.status(201).json({
            message: 'Successfully create user_game_biodata'
          })
      } catch (err) {
        next(err)
      }
    }
    static async update(req, res, next) {
        try {
            const user_game_biodata = await User_game_biodata.findOne({
              where: {
                id: req.params.id,
                user_game_id: req.user_game.id
              }
            })
      
            if (!user_game_biodata) {
              throw {
                status: 404,
                message: 'User_game_biodata not found'
              }
            } else {
              await User_game_biodata.update(req.body, {
                where: {
                  user_game_id: req.user_game.id
                }
              })
              res.status(200).json({
                message: 'Successfully update user_game_biodata'
              })
            }
          } catch (err) {
            next(err)
          }
    }
    static async delete(req, res, next) {
        try {
            const user_game_biodata = await User_game_biodata.findOne({
              where: {
                id: req.params.id,
                user_game_id: req.user_game.id
              }
            })
            if (!user_game_biodata) {
              throw {
                status: 404,
                message: 'User_game_biodata not found'
              }
            } else {
              await User_game_biodata.destroy({
                where: {
                  user_game_id: req.user_game.id
                }
              })
              res.status(200).json({
                message: 'Succesfully delete user_game_biodata'
              })
            }
          } catch (err) {
            next(err)
          }
    }
}

module.exports = User_game_biodataController