const {User_game_historie} = require('../models')

class User_game_historieController {
    
    static async list(req, res, next) {
        User_game_historie.findAll( { attributes: ['id', 'time_list', 'score', 'user_game_id'] } )
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            res.status(500).json(error)
        })
    }
    static async getById(req, res,next) {
        try {
            const user_game_historie = await User_game_historie.findOne({
              where: {
                  id: req.params.id,
                  user_game_id: req.user_game.id
              }
            })
            console.log(user_game_historie)
            if (!user_game_historie) {
              throw {
                status: 404,
                message: 'User_game_historie not found'
              }
            } else {
              res.status(200).json(user_game_historie)
            }
          } catch (err) {
            next(err)
          }
    }
    static async create(req, res,next) {
      try {
        await User_game_historie.create({
            time_list: req.body.time_list,
            score: req.body.score,
            user_game_id: req.body.user_game_id,
        })
        res.status(201).json({
            message: 'Successfully create user_game_historie'
          })
      } catch (err) {
        next(err)
      }
    }
    static async update(req, res,next) {
        try {
            const user_game_historie = await User_game_historie.findOne({
              where: {
                user_game_id: req.user_game.id
              }
            })
      
            if (!user_game_historie) {
              throw {
                status: 404,
                message: 'User_game_historie not found'
              }
            } else {
              await User_game_historie.update(req.body, {
                where: {
                  user_game_id: req.user_game.id
                }
              })
              res.status(200).json({
                message: 'Successfully update user_game_historie'
              })
            }
          } catch (err) {
            next(err)
          }
    }
    static async delete(req, res,next) {
        try {
            const user_game_historie = await User_game_historie.findOne({
              where: {
                id: req.params.id,
                user_game_id: req.user_game.id
              }
            })
            if (!user_game_historie) {
              throw {
                status: 404,
                message: 'User_game_historie not found'
              }
            } else {
              await User_game_historie.destroy({
                where: {
                  user_game_id: req.user_game.id
                }
              })
              res.status(200).json({
                message: 'Succesfully delete user_game_historie'
              })
            }
          } catch (err) {
            next(err)
          }
    }
}

module.exports = User_game_historieController