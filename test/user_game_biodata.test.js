const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface} = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require ('bcryptjs')
const app = require('../app')

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("ryanpw", salt)
    const hash2 = bcrypt.hashSync("yukii", salt)
    await queryInterface.bulkInsert('User_games', [
        {
            username: "ryanz",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date()
        },
        {
            username: "yuki",
            password: hash2,
            createdAt: new Date(),
            updatedAt: new Date()
        }
    ])
    await queryInterface.bulkInsert('User_game_biodata', [
        {
          id: 1,
          name: "febryan",
          date_of_birth: "2000-02-22",
          email: "febryanz2k@gmail.com",
          gender: "male",
          country: "indonesia",
          user_game_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ])
      token = jwt.sign({
        id: 1,
        username: 'ryanz'
      }, 'qweqwe')
    
      token2 = jwt.sign({
        id: 2,
        username: "yuki"
      }, 'qweqwe')
})

afterEach(async () => {
    await queryInterface.bulkDelete('User_games', {}, { truncate: true, restartIdentity: true })
    await queryInterface.bulkDelete('User_game_biodata', {}, { truncate: true, restartIdentity: true })
})

describe('GET User_game_biodata/list', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game_biodata')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(Array.isArray(res.body)).toBe(true)
                done()
            }
        })
    })
})

describe('GET User_game_biodata/getById', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game_biodata/1')
        .set("authorization", token)
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('id')
                expect(res.body).toHaveProperty('name')
                expect(res.body).toHaveProperty('date_of_birth')
                expect(res.body).toHaveProperty('email')
                expect(res.body).toHaveProperty('gender')
                expect(res.body).toHaveProperty('country')
                expect(res.body).toHaveProperty('user_game_id')
                done()
            }
        })
    })
    it('No auth', (done) => {
        request(app)
        .get('/user_game_biodata/1')
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
          }
        })
      })
      it('Not found', (done) => {
        request(app)
        .get('/user_game_biodata/100')
        .set('authorization', token)
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(404)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('User_game_biodata not found')
            done()
          }
        })
      })
})

describe('POST User_game_biodata/create', () => {
    it('response success', (done) => {
      request(app)
      .post('/user_game_biodata')
      .set("authorization", token2)
      .send({
        name: "yuki",
        date_of_birth: "2002-02-22",
        email: "yukii@gmail.com",
        gender: "female",
        country: "japan",
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Successfully create user_game_biodata')
          done()
        }
      })
    })
    it('response failed', (done) => {
      request(app)
      .post('/user_game_biodata')
      .set("authorization", token)
      .send({
        name: "yuki",
        date_of_birth: "2002-02-22",
        email: "yukii@gmail.com",
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(400)
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .post('/user_game_biodata')
      .send({
        name: "yuki",
        date_of_birth: "2002-02-22",
        email: "yukii@gmail.com",
        gender: "female",
        country: "japan",
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
})

describe('PUT User_game_biodata/update', () => {
  it('Success update', (done) => {
      request(app)
      .put('/user_game_biodata/1')
      .set('authorization', token)
      .send({
        name: "yuki",
        date_of_birth: "2002-02-22",
        email: "yukii@gmail.com",
        gender: "female",
        country: "japan",
        user_game_id: 1
      })
      .end( (err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(200)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('Successfully update user_game_biodata')
              done()
          }
      })
  })
  it('Failed update', (done) => {
    request(app)
    .put('/user_game_biodata/1')
    .set('authorization', token)
    .send({
      name: "yuki",
      date_of_birth: "2002-02-22",
      email: "yukii@gmail.com",
      gender: "female",
      country: "japan",
      user_game_id: 10
    })
    .end( (err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(400)
            done()
          }
      })
  })
  it('No auth', (done) => {
    request(app)
    .put('/user_game_biodata/1')
    .send({
      name: "yuki",
      date_of_birth: "2002-02-22",
      email: "yukii@gmail.com",
      gender: "female",
      country: "japan",
      user_game_id: 1
    })
    .end( (err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
          }
      })
  })  
})

describe('DELETE User_game_biodata/delete', () => {
  it('response success', (done) => {
      request(app)
      .delete('/user_game_biodata/1')
      .set('authorization', token)
      .end((err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(200)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('Succesfully delete user_game_biodata')
              done()
          }
      })
  })
  it('No auth', (done) => {
    request(app)
    .delete('/user_game_biodata/1')
    .end((err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
        }
    })
})
  it('response failed', (done) => {
      request(app)
      .delete('/user_game_biodata/10')
      .set('authorization', token)
      .end((err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(404)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('User_game_biodata not found')
              done()
          }
      })
  })
})