const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface} = sequelize
const bcrypt = require ('bcryptjs')
const app = require('../app')

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("ryanpw", salt)
    const hash2 = bcrypt.hashSync("yukii", salt)
    await queryInterface.bulkInsert('User_games', [
        {
            username: "ryanz",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date()
        },
        {
            username: "yuki",
            password: hash2,
            createdAt: new Date(),
            updatedAt: new Date()
        }
    ])
})

afterEach(async () => {
    await queryInterface.bulkDelete('User_games', {}, { truncate: true, restartIdentity: true })
})

describe('Login API', () => {
    it('Success', (done) => {
        request(app)
        .post('/user_game/login')
        .send({
            username: "ryanz",
            password: "ryanpw"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('token')
                done()
            }
        })
    })
    it('Wrong password', (done) => {
        request(app)
        .post('/user_game/login')
        .send({
            username: "ryanz",
            password: "ryanpw1"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Invalid username or password')
                done()
            }
        })
    })
    it('Wrong username', (done) => {
        request(app)
        .post('/user_game/login')
        .send({
            username: "ryan",
            password: "ryanpw"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Invalid username or password')
                done()
            }
        })
    })
})

describe('GET User_game/list', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(Array.isArray(res.body)).toBe(true)
                done()
            }
        })
    })
})

describe('GET User_game/getById', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game/2')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('id')
                expect(res.body).toHaveProperty('username')
                expect(res.body).toHaveProperty('password')
                done()
            }
        })
    })
})

describe('POST User_game/create', () => {
    it('Success create', (done) => {
        request(app)
        .post('/user_game')
        .send({
            username: "killua",
            password: "killua22"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(201)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succesfully create user_game')
                done()
            }
        })
    })
    it('User_game Failed create', (done) => {
        request(app)
        .post('/user_game')
        .send({
            username: "killua"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(400)
                done()
            }
        })
    })
})

describe('POST User_game/login', () => {
    it('Success login', (done) => {
        request(app)
        .post('/user_game/login')
        .send({
            username: "ryanz",
            password: "ryanpw"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('token')
                done()
            }
        })
    })
    it('User_game Failed login', (done) => {
        request(app)
        .post('/user_game/login')
        .send({
            username: "ryanz",
            password: "ryanpw1"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Invalid username or password')
                done()
            }
        })
    })
 })

describe('PUT User_game/update', () => {
    it('Success update', (done) => {
        request(app)
        .put('/user_game/2')
        .send({
            username: "yuki",
            password: "yukiii"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Successfully update user_game')
                done()
            }
        })
    })
    it('User_game Failed update', (done) => {
        request(app)
        .put('/user_game/3')
        .send({
            username: "yuki",
            password: "yukii"
        })
        .end( (err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(404)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('User_game not found')
                done()
            }
        })
    })
})

describe('DELETE User_game/delete', () => {
    it('response success', (done) => {
        request(app)
        .delete('/user_game/1')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succesfully delete user_game')
                done()
            }
        })
    })
    it('User_game response failed', (done) => {
        request(app)
        .delete('/user_game/10')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(404)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('User_game not found')
                done()
            }
        })
    })
})