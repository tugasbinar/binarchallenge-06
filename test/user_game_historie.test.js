const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface} = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require ('bcryptjs')
const app = require('../app')

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("ryanpw", salt)
    const hash2 = bcrypt.hashSync("yukii", salt)
    await queryInterface.bulkInsert('User_games', [
        {
            username: "ryanz",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date()
        },
        {
            username: "yuki",
            password: hash2,
            createdAt: new Date(),
            updatedAt: new Date()
        }
    ])
    await queryInterface.bulkInsert('User_game_histories', [
        {
          id: 1,
          time_list: "00:30:10",
          score: 90,
          user_game_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ])
      token = jwt.sign({
        id: 1,
        username: 'ryanz'
      }, 'qweqwe')
    
      token2 = jwt.sign({
        id: 2,
        username: "yuki"
      }, 'qweqwe')
})

afterEach(async () => {
    await queryInterface.bulkDelete('User_games', {}, { truncate: true, restartIdentity: true })
    await queryInterface.bulkDelete('User_game_histories', {}, { truncate: true, restartIdentity: true })
})

describe('GET User_game_historie/list', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game_historie')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(Array.isArray(res.body)).toBe(true)
                done()
            }
        })
    })
})

describe('GET User_game_historie/getById', () => {
    it('response success', (done) => {
        request(app)
        .get('/user_game_historie/1')
        .set("authorization", token)
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('id')
                expect(res.body).toHaveProperty('time_list')
                expect(res.body).toHaveProperty('score')
                expect(res.body).toHaveProperty('user_game_id')
                done()
            }
        })
    })
    it('No auth', (done) => {
        request(app)
        .get('/user_game_historie/1')
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
          }
        })
      })
      it('Not found', (done) => {
        request(app)
        .get('/user_game_historie/100')
        .set('authorization', token)
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(404)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('User_game_historie not found')
            done()
          }
        })
      })
})

describe('POST User_game_historie/create', () => {
    it('response success', (done) => {
      request(app)
      .post('/user_game_historie')
      .set("authorization", token2)
      .send({
        time_list: "00:30:10",
        score: 90,
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Successfully create user_game_historie')
          done()
        }
      })
    })
    it('response failed', (done) => {
      request(app)
      .post('/user_game_historie')
      .set("authorization", token)
      .send({
        time_list: "00:30:10",
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(400)
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .post('/user_game_historie')
      .send({
        time_list: "00:30:10",
        score: 90,
        user_game_id: 2
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
})

describe('PUT User_game_historie/update', () => {
  it('Success update', (done) => {
      request(app)
      .put('/user_game_historie/1')
      .set('authorization', token)
      .send({
        time_list: "00:30:10",
        score: 90,
        user_game_id: 1
      })
      .end( (err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(200)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('Successfully update user_game_historie')
              done()
          }
      })
  })
  it('Failed update', (done) => {
    request(app)
    .put('/user_game_historie/1')
    .set('authorization', token)
    .send({
        time_list: "00:30:10",
        score: 90,
        user_game_id: 10
    })
    .end( (err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(400)
            done()
          }
      })
  })
  it('No auth', (done) => {
    request(app)
    .put('/user_game_historie/1')
    .send({
        time_list: "00:30:10",
        score: 90,
        user_game_id: 1
    })
    .end( (err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
          }
      })
  })  
})

describe('DELETE User_game_historie/delete', () => {
  it('response success', (done) => {
      request(app)
      .delete('/user_game_historie/1')
      .set('authorization', token)
      .end((err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(200)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('Succesfully delete user_game_historie')
              done()
          }
      })
  })
  it('No auth', (done) => {
    request(app)
    .delete('/user_game_historie/1')
    .end((err, res) => {
        if (err) {
            done(err)
        } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Unauthorized request')
            done()
        }
    })
})
  it('response failed', (done) => {
      request(app)
      .delete('/user_game_historie/10')
      .set('authorization', token)
      .end((err, res) => {
          if (err) {
              done(err)
          } else {
              expect(res.status).toBe(404)
              expect(res.body).toHaveProperty('message')
              expect(res.body.message).toBe('User_game_historie not found')
              done()
          }
      })
  })
})