'use strict';
const fs = require('fs')

module.exports = {
  async up (queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync('./user_game_biodata.json', 'utf-8'))
    const user_game_biodatas = data.map((element) => {
      return {
        name: element.name,
        date_of_birth: element.date_of_birth,
        email: element.email,
        gender: element.gender,
        country: element.country,
        user_game_id: element.user_game_id,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    await queryInterface.bulkInsert('User_game_biodata', user_game_biodatas);
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('User_game_biodata', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
