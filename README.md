### Link ERD
```
https://dbdiagram.io/d/6239a63dbed6183873d792bc
```

### Link Postman
```
https://documenter.getpostman.com/view/18376792/UyrAEwbn
```

### Link Heroku git URL
```
https://git.heroku.com/binarchallenge-06.git
```

### Link Heroku App
```
https://binarchallenge-06.herokuapp.com/
```

### Link Gitlab Group
```
https://gitlab.com/tugasbinar
```

### Link Gitlab
```
https://gitlab.com/tugasbinar/binarchallenge-06.git
```